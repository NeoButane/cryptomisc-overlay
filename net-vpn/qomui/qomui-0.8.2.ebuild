# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=( python3_{5,6,7,8} )

inherit distutils-r1 desktop linux-info

if [[ ${PV} == 9999 ]]; then
	EGIT_REPO_URI="https://github.com/corrad1nho/${PN}.git"
	EGIT_BRANCH="master"
	inherit git-r3
elif [[ ${PV} == 99999 ]]; then
	EGIT_REPO_URI="https://github.com/corrad1nho/${PN}.git"
	EGIT_BRANCH="develop"
	inherit git-r3
else
	KEYWORDS="~amd64"
	SRC_URI="https://github.com/corrad1nho/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
fi

DESCRIPTION="Qomui (Qt OpenVPN Management UI) is a provider-independent OpenVPN/WireGuard GUI for GNU/Linux"
HOMEPAGE="https://github.com/corrad1nho/qomui"
LICENSE="GPL-3+"
SLOT="0"
IUSE=""

DEPEND="
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtwidgets:5
	>=dev-python/PyQt5-5[${PYTHON_USEDEP}]
	>=dev-python/setuptools-40.6.3[${PYTHON_USEDEP}]
	>=dev-python/beautifulsoup-4.5.1[${PYTHON_USEDEP}]
	>=dev-python/pexpect-4.6.0[${PYTHON_USEDEP}]
	>=dev-python/psutil-5.5.0[${PYTHON_USEDEP}]
	>=dev-python/requests-2.21.0[${PYTHON_USEDEP}]
	>=dev-python/lxml-4.2.5[${PYTHON_USEDEP}]
"
RDEPEND="${DEPEND}"

CONFIG_CHECK="~TUN"

PATCHES="${FILESDIR}/qomui-0.8.2-Remove-chmod-from-Python-setup.patch"

pkg_setup()  {
        linux-info_pkg_setup
}

python_install_all() {
        local DOCS=( CHANGELOG.md )

        distutils-r1_python_install_all

	chmod 665 -R "${D}/usr/share/qomui" || die
}

#src_install() {

	# For Makefiles that don't make proper use of DESTDIR, setting
	# prefix is often an alternative.  However if you do this, then
	# you also need to specify mandir and infodir, since they were
	# passed to ./configure as absolute paths (overriding the prefix
	# setting).
	#emake \
	#	prefix="${D}"/usr \
	#	mandir="${D}"/usr/share/man \
	#	infodir="${D}"/usr/share/info \
	#	libdir="${D}"/usr/$(get_libdir) \
	#	install
	# Again, verify the Makefiles!  We don't want anything falling
	# outside of ${D}.
#}
