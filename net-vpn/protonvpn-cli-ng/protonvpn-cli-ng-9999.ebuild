# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

PYTHON_COMPAT=( python3_{5,6,7} )

DISTUTILS_USE_SETUPTOOLS=rdepend

inherit distutils-r1 linux-info

if [[ ${PV} == "9999" ]]; then
        EGIT_REPO_URI="https://github.com/ProtonVPN/${PN}.git"
        inherit git-r3
else
        SRC_URI="https://github.com/ProtonVPN/${PN}/archive/v${PV}.tar.gz"
        KEYWORDS="~amd64"
fi

DESCRIPTION="ProtonVPN-CLI-ng is a command-line interface written in Python for the ProtonVPN service"
HOMEPAGE="https://github.com/ProtonVPN/protonvpn-cli-ng"
LICENSE="GPL-3"
SLOT="0"

RDEPEND="
	>=dev-python/docopt-0.6.2-r2[${PYTHON_USEDEP}]
	>=dev-python/pythondialog-3.3.0:0[${PYTHON_USEDEP}]
	>=dev-python/requests-2.21.0-r1[${PYTHON_USEDEP}]
	>=net-vpn/openvpn-2.4.6
"
DEPEND="
	${RDEPEND}
	>=dev-python/setuptools-40.6.3[${PYTHON_USEDEP}]
"

CONFIG_CHECK="~TUN"


pkg_setup() {
	linux-info_pkg_setup
}


python_install_all() {
	local DOCS=( AUTHORS CHANGELOG.md README.md USAGE.md )

	distutils-r1_python_install_all
}
