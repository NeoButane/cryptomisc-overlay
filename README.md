# cryptomisc-overlay

Gentoo ebuilds related to cryptocurrency tools or privacy, mainly for the amd64 architecture.

```
[cryptomisc-overlay]
location   = /var/db/repos/cryptomisc-overlay
sync-uri   = https://gitlab.com/NeoButane/cryptomisc-overlay.git
sync-type  = git
priority   = 101
```
