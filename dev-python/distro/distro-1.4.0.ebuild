# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

PYTHON_COMPAT=( python3_{6,7,8} )

inherit distutils-r1

DESCRIPTION="Reliable machine-readable Linux distribution information for Python"
HOMEPAGE="https://distro.readthedocs.io/"
SRC_URI="https://github.com/nir0s/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
#mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

SLOT="0"
LICENSE="Apache-2.0"
KEYWORDS="~amd64 ~arm64 ~x86 ~amd64-linux ~x86-linux"
IUSE="test"

RDEPEND=""
DEPEND="${RDEPEND}
        dev-python/setuptools[${PYTHON_USEDEP}]
        test? (
                dev-python/pytest[${PYTHON_USEDEP}]
                dev-python/pytest-cov[${PYTHON_USEDEP}]
		dev-python/sphinx[${PYTHON_USEDEP}]
        )"

distutils_enable_tests pytest
