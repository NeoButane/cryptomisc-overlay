# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

PYTHON_COMPAT=( python3_{6,7,8} )

MY_PN="${PN}-clients"
MY_PV="1.0.1"
MY_P="${MY_PN}-${MY_PV}"

inherit distutils-r1

DESCRIPTION="API client for the Deribit API in Python"
HOMEPAGE="https://github.com/deribit/deribit-api-clients"
IUSE="+doc test"
REQUIRED_USE="doc ${PYTHON_REQUIRED_USE}"

if [[ ${PV} == 9999* ]]; then
        EGIT_REPO_URI="https://github.com/deribit/${MY_PN}.git"
        inherit git-r3
else
	KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"
        SRC_URI="https://github.com/deribit/${MY_PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
fi

LICENSE=""
SLOT="0"

S="${WORKDIR}/${MY_P}/python"
#	${FILESDIR}/0001-f.patch

PATCHES="
	${FILESDIR}/deribit-api-1.0.0-fix-setup_py.patch
	${FILESDIR}/deribit-api-1.0.0-Rename-package-to-deribit_api-from-openapi_client.patch
	"

RDEPEND=""
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
	>=dev-python/urllib3-1.24.2[${PYTHON_USEDEP}]
	>=dev-python/six-1.12.0[${PYTHON_USEDEP}]
	dev-python/certifi[${PYTHON_USEDEP}]
	dev-python/python-dateutil[${PYTHON_USEDEP}]
	test? (
	>=dev-python/coverage-4.2[${PYTHON_USEDEP}]
	>=dev-python/nose-1.3.7-r3[${PYTHON_USEDEP}]
	>=dev-python/pluggy-0.5.2[${PYTHON_USEDEP}]
	>=dev-python/py-1.4.34[${PYTHON_USEDEP}]
	)"


pkg_postinst() {
	if use doc ; then
		dodoc -r "${S}/docs"
	fi

	ewarn "The Python package has been renamed from openapi_client to deribit_api. The documentation has been changed to reflect this."
}

distutils_enable_tests nose
