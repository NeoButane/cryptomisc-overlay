# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

PYTHON_COMPAT=( python3_{6,7,8} )

inherit distutils-r1

DESCRIPTION="An implementation of the WebSocket Protocol (RFC 6455 & 7692)"
HOMEPAGE="https://github.com/man-c/pycoingecko.git https://pypi.org/project/pycoingecko/"
SRC_URI="https://github.com/man-c/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
#mirror://pypi/${P:0:1}/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"
IUSE="test"

RDEPEND=""
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
	dev-python/requests[${PYTHON_USEDEP}]
	test? (
		dev-python/responses[${PYTHON_USEDEP}]
		dev-python/pytest-localserver[${PYTHON_USEDEP}]
	)"

python_test() {
	pytest ${S}/tests/test_api.py
}

distutils_enable_tests pytest
